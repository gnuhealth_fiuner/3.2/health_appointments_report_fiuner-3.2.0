# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_appointments_report import *
from .report import *

def register():
    Pool.register(
        AppointmentReport,
        module='health_appointments_report_fiuner', type_='model')
    Pool.register(
        ReporteCitas,
        module='health_appointments_report_fiuner', type_='report')
