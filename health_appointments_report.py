#-*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta

__all__ = ['AppointmentReport']


class AppointmentReport(metaclass=PoolMeta):
    
    __name__ = 'gnuhealth.appointment.report'

    fecha_nacimiento = fields.Function(fields.Char('Fec. Nac'), 'get_fecha_nacimiento')
    
    def get_fecha_nacimiento(self, patient):
        if  self.patient.name.dob:
            return self.patient.name.dob
        else:
            return ''

    hc = fields.Function(fields.Char('HC'), 'get_hc')

    def get_hc(self, patient):
        if self.patient.hc:
            return  self.patient.hc
        else:
            return ''






